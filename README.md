# dwm scripts

Simple scripts to display battery status, volume control, weather, email, date and time in DWM status bar.

File "xinitrc" should be copied to ~/.xinitrc"

Besides the scripts an executable (xkb3) is used for the keyboard layout switcher.

