#!/bin/sh

export XBASE=1480
#export XBASE=3300	# dual monitor

killall dzen2
killall dzen-local

~/scripts/battery &
~/scripts/weather &
~/scripts/volume &
~/scripts/mutt-mail &
#~/scripts/mutt+thund-mail &	# thunderbird
~/scripts/date_time &
~/scripts/pidgin &
~/scripts/keyboard
~/scripts/xkb 0 &

