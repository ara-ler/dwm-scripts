// provides X keyboard layout switching between 3 layouts set
// in either in xorg.conf or by setxkbmap
// accept 1, 2 or 3 arguments - integers 0, 1, 2 corresponding to the layouts
// 1 argument - switches to that layout
// 2 arguments - toggles those layouts
// 3 arguments - increments layout by 1 every time or wrap around
// compile:  gcc -Wall -lxkbfile xkb3.c -o xkb3

#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/XKBlib.h>
#include <X11/extensions/XKB.h>
#include <X11/extensions/XKBstr.h>
#define MIN_LAYOUT 0
#define MAX_LAYOUT 2
#define LAYOUT_0   0
#define LAYOUT_1   1
#define LAYOUT_2   2

int main(int argc, char *argv[])
{
   Display *dpy;
   int res;
   XkbStateRec state;
   int layout0, layout1, layout2;
   
   if (argc < 2) {printf("Usage: xkb3 layout0 [layout1]\n"); return(0);}
   
   dpy = XOpenDisplay(NULL);
   if (!dpy) {printf("Can't open display\n"); return(1);}

   res = XkbQueryExtension(dpy, NULL, NULL, NULL, NULL, NULL);
   if (!res) {printf("Can't init XKB\n"); return(1);}

   XkbGetState(dpy, XkbUseCoreKbd, &state);
   //Debug:
   //printf("state.group=%d\n", state.group);
   
   switch (argc)
   {
     case 2:
	layout0 = atoi(argv[1]);
	if (layout0 < MIN_LAYOUT || layout0 > MAX_LAYOUT) {printf("Argument is not within range of 0...2\n");return(0);}
  	XkbLockGroup(dpy, XkbUseCoreKbd, layout0);
	break;
     case 3:
	layout0 = atoi(argv[1]);
	if (layout0 < MIN_LAYOUT || layout0 > MAX_LAYOUT) {printf("Argument #1 is not within range of 0...2\n");return(0);}
	layout1 = atoi(argv[2]);
	if (layout1 < MIN_LAYOUT || layout1 > MAX_LAYOUT) {printf("Argument #2 is not within range of 0...2\n");return(0);}
	switch (state.group)
	{
	   case	LAYOUT_0:
		XkbLockGroup(dpy, XkbUseCoreKbd, layout1);
		break;
	   case LAYOUT_1:
		XkbLockGroup(dpy, XkbUseCoreKbd, layout0);
		break;
	   default:
		XkbLockGroup(dpy, XkbUseCoreKbd, 0);
	}
	break;
     case 4:
	layout0 = atoi(argv[1]);
	if (layout0 < MIN_LAYOUT || layout0 > MAX_LAYOUT) {printf("Argument #1 is not within range of 0...2\n");return(0);}
	layout1 = atoi(argv[2]);
	if (layout1 < MIN_LAYOUT || layout1 > MAX_LAYOUT) {printf("Argument #2 is not within range of 0...2\n");return(0);}
	layout2 = atoi(argv[3]);
	if (layout2 < MIN_LAYOUT || layout2 > MAX_LAYOUT) {printf("Argument #3 is not within range of 0...2\n");return(0);}
  	XkbLockGroup(dpy, XkbUseCoreKbd, (state.group + 1)%(MAX_LAYOUT + 1));
     default:
	printf("Too many arguments, will do nothing\n");
   }
   
   XkbGetState(dpy, XkbUseCoreKbd, &state);
   XCloseDisplay(dpy);
   printf("%d\n", state.group); 

   return(0);
}

